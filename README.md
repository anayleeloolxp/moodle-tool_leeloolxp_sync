#Leeloo LXP Synchronizer

This tool will synchronize data from Moodle LMS to Leeloo LXP.
For your convenience, you can also use the Leeloo LXP Synchronization block, for quick and easy settings.


It allows administrators and authorized personnel to:
- Use the advanced automation and bulk actions tools (the LMS Toolkit)
o Courses and Activities/Resources Calendar Snapshot
o Activities and Resources One-pager quick editor
- Activate and update the Student Dashboard and the Teacher Dashboard
- Use the Student Relationship Management module
- Use the Gamification and the Actionable Learning Analytics modules
- Use the Time Tracking features (additional plugins are required)

This plugin will synchronize:
- Courses
- Sections
- Activities and Resources
- Enrolled Users and Roles
- Groups

Note: you can choose which categories, courses, sections and activities/resources you wish to sync.

Setup:
Go to Admin > Plugins > Leeloo LXP Activities and Resource Synchronizer and input your provided license key.

Note that this plugin and most of the features associated with it are free to use.