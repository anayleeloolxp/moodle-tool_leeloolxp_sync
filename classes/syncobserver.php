<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Admin settings and defaults
 *
 * @package tool_leeloolxp_sync
 * @copyright  2020 Leeloo LXP (https://leeloolxp.com)
 * @author Leeloo LXP <info@leeloolxp.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace tool_leeloolxp_sync;

use core_user;
use curl;
use moodle_url;

defined('MOODLE_INTERNAL') || die();

/**
 * Plugin to sync users on new enroll, groups, trackign of activity view to LeelooLXP account of the Moodle Admin
 */
class syncobserver {

    /**
     * Triggered when user view course content.
     *
     * @param \core\event\course_module_viewed $events
     */
    public static function viewed_activity(\core\event\course_module_viewed $events) {

        global $USER;

        global $CFG;

        require_once($CFG->dirroot . '/lib/filelib.php');

        $configenroll = get_config('tool_leeloolxp_sync');

        $liacnsekey = $configenroll->leeloolxp_synclicensekey;

        $postdata = array('license_key' => $liacnsekey);

        $url = 'https://leeloolxp.com/api_moodle.php/?action=page_info';

        $curl = new curl;

        $options = array(

            'CURLOPT_RETURNTRANSFER' => true,

            'CURLOPT_HEADER' => false,

            'CURLOPT_POST' => count($postdata),

        );

        if (!$output = $curl->post($url, $postdata, $options)) {
            return true;
        }

        $infoteamnio = json_decode($output);

        $teamniourl = $infoteamnio->data->install_url;

        $eventdata = $events->get_data();

        $alldetail = json_encode($eventdata);

        $userid = $eventdata['userid'];

        $courseid = $eventdata['courseid'];

        $contextinstanceid = $eventdata['contextinstanceid'];

        $component = $eventdata['component'];

        $action = $eventdata['action'];

        $postdata = array(
            'moodle_user_id' => $userid,
            'course_id' => $courseid,
            'activity_id' => $contextinstanceid,
            'mod_name' => $component,
            'user_email' => $USER->email,
            'action' => $action,
            'detail' => $alldetail,
        );

        $url = $teamniourl . '/admin/sync_moodle_course/update_viewed_log';

        $curl = new curl;

        $options = array(

            'CURLOPT_RETURNTRANSFER' => true,

            'CURLOPT_HEADER' => false,

            'CURLOPT_POST' => count($postdata),

        );
    }

    /**
     * Triggered when base.
     *
     * @param \core\event\base $events
     */
    public static function badge_createdd(\core\event\base $events) {

        global $USER;

        global $CFG;
        global $DB;


        require_once($CFG->dirroot . '/lib/filelib.php');

        $configenroll = get_config('tool_leeloolxp_sync');

        $liacnsekey = $configenroll->leeloolxp_synclicensekey;

        $postdata = array('license_key' => $liacnsekey);

        $url = 'https://leeloolxp.com/api_moodle.php/?action=page_info';

        $curl = new curl;

        $options = array(

            'CURLOPT_RETURNTRANSFER' => true,

            'CURLOPT_HEADER' => false,

            'CURLOPT_POST' => count($postdata),

        );

        if (!$output = $curl->post($url, $postdata, $options)) {
            return true;
        }

        $infoteamnio = json_decode($output);

        $teamniourl = $infoteamnio->data->install_url;

        $eventdata = $events->get_data();

        $alldetail = json_encode($eventdata);

        $userid = $eventdata['userid'];

        $courseid = $eventdata['courseid'];

        $contextinstanceid = $eventdata['objectid'];

        $component = $eventdata['target'];

        $action = $eventdata['action'];

        $eventname = $eventdata['eventname'];

        $eventsdata = $events->get_data();

        // echo "<pre>";print_r($eventsdata);die;

        /* Store data in a text file for debugging */

        /*$ext = ".txt";
        $namef = 'test_'.strtotime(date('Y-m-d H:i:s'));
        $filename = $namef.$ext;
        file_put_contents(dirname(__FILE__) . $filename, print_r($eventsdata,true) );*/


        // sync tag to leeloo , tag deleted
        if($eventname == '\core\event\tag_deleted') {

            $eventsdata = $events->get_data();

            $tagdata = [
                'id' => $eventsdata['objectid']
            ];

            $tagdata = json_encode($tagdata);

            $posttagdata = '&tagdata=' . $tagdata;

            $url = $teamniourl . '/admin/sync_moodle_course/delete_tags';
            $curl = new curl;
            $options = array(
                'CURLOPT_RETURNTRANSFER' => true,
                'CURLOPT_HEADER' => false,
                'CURLOPT_POST' => count($posttagdata),
            );
            $output = $curl->post($url, $posttagdata, $options);
        }


        // sync tag to leeloo , tag flag change
        if($eventname == '\core\event\tag_unflagged' || $eventname == '\core\event\tag_flagged') {

            $id = $eventsdata['objectid'];

            if ($eventname == '\core\event\tag_unflagged') {
                $flag = 0;
            } else {
                $flag = 1;
            }

            $tagdbdata = $DB->get_record_sql("select isstandard,description from {tag} where id = '$id' ");

            $eventsdata = $events->get_data();

            $tagdata = [
                'flag' => $flag,
                'email' => $USER->email,
                'name' => $eventsdata['other']['name'],
                'isstandard' => $tagdbdata->isstandard,
                'description' => $tagdbdata->description,
                'is_update' => '1',
                'id' => $id
            ];

            $tagdata = json_encode($tagdata);

            $posttagdata = '&tagdata=' . $tagdata;

            $url = $teamniourl . '/admin/sync_moodle_course/sync_tags';
            $curl = new curl;
            $options = array(
                'CURLOPT_RETURNTRANSFER' => true,
                'CURLOPT_HEADER' => false,
                'CURLOPT_POST' => count($posttagdata),
            );
            $output = $curl->post($url, $posttagdata, $options);
        }

        // sync tag to leeloo , created tag from popup
        if($eventname == '\core\event\tag_created') {

            $eventsdata = $events->get_data();

            $id = $eventsdata['objectid'];

            $tagdbdata = $DB->get_record_sql("select isstandard,description from {tag} where id = '$id' ");

            if (!empty($tagdbdata->isstandard)) {

                $tagdata = [
                    'email' => $USER->email,
                    'name' => $eventsdata['other']['name'],
                    'isstandard' => '1',
                    'description' => '',
                    'id' => $id
                ];

                $tagdata = json_encode($tagdata);

                $posttagdata = '&tagdata=' . $tagdata;

                $url = $teamniourl . '/admin/sync_moodle_course/sync_tags';
                $curl = new curl;
                $options = array(
                    'CURLOPT_RETURNTRANSFER' => true,
                    'CURLOPT_HEADER' => false,
                    'CURLOPT_POST' => count($posttagdata),
                );
                $output = $curl->post($url, $posttagdata, $options);
                // echo "<pre>";print_r($output);die;
            }
        }

        // sync tag to leeloo , tag updated
        if($eventname == '\core\event\tag_updated') {

            $id = $eventsdata['objectid'];

            $tagdbdata = $DB->get_record_sql("select isstandard,description from {tag} where id = '$id' ");

            $eventsdata = $events->get_data();

            $tagdata = [
                'email' => $USER->email,
                'name' => $eventsdata['other']['name'],
                'isstandard' => $tagdbdata->isstandard,
                'description' => $tagdbdata->description,
                'is_update' => '1',
                'id' => $id
            ];

            $tagdata = json_encode($tagdata);

            $posttagdata = '&tagdata=' . $tagdata;

            $url = $teamniourl . '/admin/sync_moodle_course/sync_tags';
            $curl = new curl;
            $options = array(
                'CURLOPT_RETURNTRANSFER' => true,
                'CURLOPT_HEADER' => false,
                'CURLOPT_POST' => count($posttagdata),
            );
            $output = $curl->post($url, $posttagdata, $options);
        }

        // sync tag to leeloo , added tag while updating tag
        if($eventname == '\core\event\tag_added') {

            $itemid = $eventsdata['other']['itemid'];

            $tagdbdata = $DB->get_record_sql("select isstandard,description from {tag} where id = '$id' ");

            $instancedata = $DB->get_record_sql("select name from {tag} where id = '$itemid' ");

            $eventsdata = $events->get_data();

            $tagdata = [
                'email' => $USER->email,
                'name' => $eventsdata['other']['tagname'],
                'instance_name' => $instancedata->name,
                'tagid' => $eventsdata['other']['tagid'],
                'itemid' => $itemid,
                'isstandard' => $tagdbdata->isstandard,
                'description' => $tagdbdata->description
            ];

            $tagdata = json_encode($tagdata);

            $posttagdata = '&tagdata=' . $tagdata;

            $url = $teamniourl . '/admin/sync_moodle_course/sync_tags_with_instance';
            $curl = new curl;
            $options = array(
                'CURLOPT_RETURNTRANSFER' => true,
                'CURLOPT_HEADER' => false,
                'CURLOPT_POST' => count($posttagdata),
            );
            $output = $curl->post($url, $posttagdata, $options);
            // echo "<pre>";print_r($output);die;
        }

        if ($eventname == '\core\event\user_graded') {
            $quizdata = json_encode($events->get_data());

            $postdataquizgrade = array('quizdata' => $quizdata);

            $url = $teamniourl . '/admin/sync_moodle_course/update_grade_quiz_grade';
            $curl = new curl;
            $options = array(
                'CURLOPT_RETURNTRANSFER' => true,
                'CURLOPT_HEADER' => false,
                'CURLOPT_POST' => count($postdataquizgrade),
            );
            $output = $curl->post($url, $postdataquizgrade, $options);
        }
        if ($eventname == '\mod_lesson\event\essay_assessed') {
            $lessiongradedata = json_encode($events->get_data());

            $objecttable = $eventdata['objecttable'];
            $objectid = $eventdata['objectid'];
            $lessionrec = $events->get_record_snapshot($objecttable, $objectid);

            $postdatalessiongrade = array('lession_grade_data' => $lessiongradedata, 'grade' => $lessionrec->grade);

            $url = $teamniourl . '/admin/sync_moodle_course/update_grade_lession_grade';
            $curl = new curl;
            $options = array(
                'CURLOPT_RETURNTRANSFER' => true,
                'CURLOPT_HEADER' => false,
                'CURLOPT_POST' => count($postdatalessiongrade),
            );
            $output = $curl->post($url, $postdatalessiongrade, $options);
        }

        if ($eventname == '\mod_assign\event\submission_graded') {
            $objecttable = $eventdata['objecttable'];
            $objectid = $eventdata['objectid'];
            $rec = $events->get_record_snapshot($objecttable, $objectid);
            $assigndatamain = json_encode($rec);
            $assigndatacommon = json_encode($events->get_data());
            $postdataassigngrade = array('assign_data_main' => $assigndatamain, 'assign_data_common' => $assigndatacommon);

            $url = $teamniourl . '/admin/sync_moodle_course/update_grade_assign_submit';
            $curl = new curl;
            $options = array(
                'CURLOPT_RETURNTRANSFER' => true,
                'CURLOPT_HEADER' => false,
                'CURLOPT_POST' => count($postdataassigngrade),
            );
            $output = $curl->post($url, $postdataassigngrade, $options);
        }

        if ($eventname == '\core\event\grade_item_updated' || $eventname == '\core\event\grade_item_created') {
            $itemdata = $events->get_grade_item();

            $itemdataonly = array(
                'courseid' => $itemdata->courseid,
                'categoryid' => $itemdata->categoryid,
                'item_category' => $itemdata->item_category,
                'parent_category' => $itemdata->parent_category,
                'itemname' => $itemdata->itemname,
                'itemtype' => $itemdata->itemtype,
                'itemmodule' => $itemdata->itemmodule,
                'iteminstance' => $itemdata->iteminstance,
                'itemnumber' => $itemdata->itemnumber,
                'iteminfo' => $itemdata->iteminfo,
                'idnumber' => $itemdata->idnumber,
                'calculation_normalized' => $itemdata->calculation_normalized,
                'formula' => $itemdata->formula,
                'gradetype' => $itemdata->gradetype,
                'grademax' => $itemdata->grademax,
                'grademin' => $itemdata->grademin,
                'scaleid' => $itemdata->scaleid,
                'scale' => $itemdata->scale,
                'outcomeid' => $itemdata->outcomeid,
                'gradepass' => $itemdata->gradepass,
                'multfactor' => $itemdata->multfactor,
                'plusfactor' => $itemdata->plusfactor,
                'aggregationcoef' => $itemdata->aggregationcoef,
                'aggregationcoef2' => $itemdata->aggregationcoef2,
                'sortorder' => $itemdata->sortorder,
                'display' => $itemdata->display,
                'decimals' => $itemdata->decimals,
                'locked' => $itemdata->locked,
                'locktime' => $itemdata->locktime,
                'needsupdate' => $itemdata->needsupdate,
                'weightoverride' => $itemdata->weightoverride,
                'dependson_cache' => $itemdata->dependson_cache,
                'optional_fields' => json_encode($itemdata->optional_fields),
                'id' => $itemdata->id,
                'timecreated' => $itemdata->timecreated,
                'timemodified' => $itemdata->timemodified,
                'hidden' => $itemdata->hidden,
            );
            $grededata = '&item_data=' . json_encode($itemdataonly);
            $gradegradedata = $DB->get_records_sql("select * from {grade_grades} where itemid = '$itemdata->id'");
            $gredegradedatastring = '&grade_grade_data=' . json_encode($gradegradedata);

            if ($itemdata->itemtype == 'category' && !empty($itemdata->iteminstance)) {
                $gradecategorydata = $DB->get_records_sql("select * from {grade_categories} where id = '$itemdata->iteminstance' ");
                $gradecategory = '&grade_category=' . json_encode($gradecategorydata);
            }
        } else {
            $grededata = '';
            $gredegradedatastring = '';
            $gradecategory = '';
        }

        if (isset($USER->email) && isset($USER->email) != '') {
            $postdata = array(
                'moodle_user_id' => $userid,
                'course_id' => $courseid,
                'activity_id' => $contextinstanceid,
                'mod_name' => $component,
                'user_email' => $USER->email,
                'action' => $action,
                'detail' => $alldetail,
                'event_name' => $eventname . $grededata . $gredegradedatastring . $gradecategory,
            );

            $url = $teamniourl . '/admin/sync_moodle_course/update_viewed_log';
            $curl = new curl;
            $options = array(

                'CURLOPT_RETURNTRANSFER' => true,

                'CURLOPT_HEADER' => false,

                'CURLOPT_POST' => count($postdata),
            );

            $output = $curl->post($url, $postdata, $options);
        }
    }

    /**
     * Triggered when badge_criteria_created.
     *
     * @param \core\event\badge_criteria_created $events
     */
    public static function badge_criteria_createdd(\core\event\badge_criteria_created $events) {

        global $USER;

        $configenroll = get_config('tool_leeloolxp_sync');

        $liacnsekey = $configenroll->leeloolxp_synclicensekey;

        $postdata = array('license_key' => $liacnsekey);

        $url = 'https://leeloolxp.com/api_moodle.php/?action=page_info';

        $curl = new curl;

        $options = array(

            'CURLOPT_RETURNTRANSFER' => true,

            'CURLOPT_HEADER' => false,

            'CURLOPT_POST' => count($postdata),

        );

        if (!$output = $curl->post($url, $postdata, $options)) {
            return true;
        }

        $infoteamnio = json_decode($output);

        $teamniourl = $infoteamnio->data->install_url;

        $eventdata = $events->get_data();

        $alldetail = json_encode($eventdata);

        $userid = $eventdata['userid'];

        $courseid = $eventdata['courseid'];

        $contextinstanceid = $eventdata['objectid'];

        $component = $eventdata['target'];

        $action = $eventdata['action'];

        $postdata = array(
            'moodle_user_id' => $userid,
            'course_id' => $courseid,
            'activity_id' => $contextinstanceid,
            'mod_name' => $component,
            'user_email' => $USER->email,
            'action' => $action,
            'detail' => $alldetail,
        );

        $url = $teamniourl . '/admin/sync_moodle_course/update_viewed_log';
    }

    /**
     * Triggered when course completed.
     *
     * @param \core\event\course_module_completion_updated $event
     */
    public static function completion_updated(\core\event\course_module_completion_updated $event) {

        global $DB;

        global $CFG;

        require_once($CFG->dirroot . '/lib/filelib.php');

        $moduleid = $event->contextinstanceid;

        $userid = $event->userid;

        $completionstate = $event->other['completionstate'];

        $user = $DB->get_record('user', array('id' => $userid));

        $configenroll = get_config('tool_leeloolxp_sync');

        $liacnsekey = $configenroll->leeloolxp_synclicensekey;

        $postdata = array('license_key' => $liacnsekey);

        $url = 'https://leeloolxp.com/api_moodle.php/?action=page_info';

        $curl = new curl;

        $options = array(

            'CURLOPT_RETURNTRANSFER' => true,

            'CURLOPT_HEADER' => false,

            'CURLOPT_POST' => count($postdata),

        );

        if (!$output = $curl->post($url, $postdata, $options)) {
            return true;
        }

        $infoteamnio = json_decode($output);

        if ($infoteamnio->status != 'false') {
            $teamniourl = $infoteamnio->data->install_url;

            $postdata = array('email' => $user->email, 'completionstate' => $completionstate, 'activity_id' => $moduleid);

            $url = $teamniourl . '/admin/sync_moodle_course/mark_completed_by_moodle_user';

            $curl = new curl;

            $options = array(

                'CURLOPT_RETURNTRANSFER' => true,

                'CURLOPT_HEADER' => false,

                'CURLOPT_POST' => count($postdata),

            );

            if (!$output = $curl->post($url, $postdata, $options)) {
                return true;
            }
        } else {

            return true;
        }

        return true;
    }

    /**
     * Triggered when user profile updated.
     *
     * @param \core\event\user_updated $event
     */
    public static function edit_user(\core\event\user_updated $event) {

        $data = $event->get_data();

        global $DB;

        global $CFG;

        require_once($CFG->dirroot . '/lib/filelib.php');

        $user = core_user::get_user($data['relateduserid'], '*', MUST_EXIST);

        $email = str_replace("'", '', $user->email);

        $configenroll = get_config('tool_leeloolxp_sync');

        $liacnsekey = $configenroll->leeloolxp_synclicensekey;

        $postdata = array('license_key' => $liacnsekey);

        $url = 'https://leeloolxp.com/api_moodle.php/?action=page_info';

        $curl = new curl;

        $options = array(

            'CURLOPT_RETURNTRANSFER' => true,

            'CURLOPT_HEADER' => false,

            'CURLOPT_POST' => count($postdata),

        );

        if (!$output = $curl->post($url, $postdata, $options)) {
            return true;
        }

        $infoteamnio = json_decode($output);

        $countries = get_string_manager()->get_list_of_countries();

        if ($infoteamnio->status != 'false') {
            $teamniourl = $infoteamnio->data->install_url;

            $userinterests = $DB->get_records_sql("SELECT * FROM {tag}

            as t JOIN {tag_instance} as ti  ON ti.tagid = t.id JOIN {user}

            as u ON u.id = ti.itemid AND ti.itemtype = 'user'

            AND u.username = '$user->username'");

            $userinterestsarr = array();

            if (!empty($userinterests)) {
                foreach ($userinterests as $value) {
                    $userinterestsarr[] = $value->name;
                }
            }

            $userinterestsstring = implode(',', $userinterestsarr);

            $lastlogin = date('Y-m-d h:i:s', $user->lastlogin);

            $fullname = ucfirst($user->firstname) . " " . ucfirst($user->middlename) . " " . ucfirst($user->lastname);

            $city = $user->city;

            $country = $countries[$user->country];

            $timezone = $user->timezone;

            $skype = $user->skype;

            $idnumber = $user->idnumber;

            $institution = $user->institution;

            $department = $user->department;

            $phone = $user->phone1;

            $moodlephone = $user->phone2;

            $address = $user->address;

            $firstaccess = $user->firstaccess;

            $lastaccess = $user->lastaccess;

            $lastlogin = $lastlogin;

            $lastip = $user->lastip;

            $interests = $userinterestsstring;

            $description = $user->description;

            $descriptionofpic = $user->imagealt;

            $alternatename = $user->alternatename;

            $webpage = $user->url;

            $sql = "SELECT ud.data  FROM {user_info_data} ud JOIN

            {user_info_field} uf ON uf.id = ud.fieldid WHERE ud.userid = :userid

            AND uf.shortname = :fieldname";

            $params = array('userid' => $user->id, 'fieldname' => 'degree');

            $degree = $DB->get_field_sql($sql, $params);

            $sql = "SELECT ud.data FROM {user_info_data} ud JOIN {user_info_field}

            uf ON uf.id = ud.fieldid WHERE ud.userid = :userid AND

            uf.shortname = :fieldname";

            $params = array('userid' => $user->id, 'fieldname' => 'Pathway');

            $pathway = $DB->get_field_sql($sql, $params);

            $imgurl = new moodle_url('/user/pix.php/' . $user->id . '/f1.jpg');

            $postdata = array(
                'email' => $email,
                'name' => $fullname,
                'city' => $city,
                'country' => $country,
                'timezone' => $timezone,
                'skype' => $skype,
                'idnumber' => $idnumber,
                'institution' => $institution,
                'department' => $department,
                'phone' => $phone,
                'moodle_phone' => $moodlephone,
                'address' => $address,
                'firstaccess' => $firstaccess,
                'lastaccess' => $lastaccess,
                'lastlogin' => $lastlogin,
                'lastip' => $lastip,
                'description' => $description,
                'description_of_pic' => $descriptionofpic,
                'alternatename' => $alternatename,
                'web_page' => $webpage,
                'img_url' => $imgurl,
                'interests' => $interests,
                'degree' => $degree,
                'pathway' => $pathway,
            );

            $url = $teamniourl . '/admin/sync_moodle_course/update_username';

            $curl = new curl;

            $options = array(

                'CURLOPT_RETURNTRANSFER' => true,

                'CURLOPT_HEADER' => false,

                'CURLOPT_POST' => count($postdata),

            );

            if (!$output = $curl->post($url, $postdata, $options)) {
                return true;
            }
        }
    }

    /**
     * Triggered when group member added.
     *
     * @param \core\event\group_member_added $events
     */
    public static function group_member_added(\core\event\group_member_added $events) {

        global $CFG;

        require_once($CFG->dirroot . '/lib/filelib.php');

        $group = $events->get_record_snapshot('groups', $events->objectid);

        $user = core_user::get_user($events->relateduserid, '*', MUST_EXIST);

        $courseid = str_replace("'", '', $courseid);

        $groupname = str_replace("'", '', $group->name);

        $configenroll = get_config('tool_leeloolxp_sync');

        $liacnsekey = $configenroll->leeloolxp_synclicensekey;

        $postdata = array('license_key' => $liacnsekey);

        $url = 'https://leeloolxp.com/api_moodle.php/?action=page_info';

        $curl = new curl;

        $options = array(

            'CURLOPT_RETURNTRANSFER' => true,

            'CURLOPT_HEADER' => false,

            'CURLOPT_POST' => count($postdata),

        );

        if (!$output = $curl->post($url, $postdata, $options)) {
            return true;
        }

        $infoteamnio = json_decode($output);

        if ($infoteamnio->status != 'false') {
            $teamniourl = $infoteamnio->data->install_url;

            $postdata = array('email' => $user->email, 'courseid' => $courseid, 'group_name' => $groupname);

            $url = 'https://leeloolxp.com/api_moodle.php/?action=page_info';

            $curl = new curl;

            $options = array(

                'CURLOPT_RETURNTRANSFER' => true,

                'CURLOPT_HEADER' => false,

                'CURLOPT_POST' => count($postdata),

            );

            if (!$output = $curl->post($url, $postdata, $options)) {
                return true;
            }
        } else {

            return true;
        }
    }

    /**
     * Triggered when new role assigned to user.
     *
     * @param \core\event\role_assigned $enrolmentdata
     */
    public static function role_assign(\core\event\role_assigned $enrolmentdata) {

        global $DB;

        global $CFG;

        require_once($CFG->dirroot . '/lib/filelib.php');

        $snapshotid = $enrolmentdata->get_data()['other']['id'];

        $snapshot = $enrolmentdata->get_record_snapshot('role_assignments', $snapshotid);

        $roleid = $snapshot->roleid;

        $usertype = '';

        $teamniorole = '';

        $user = $DB->get_record('user', array('id' => $enrolmentdata->relateduserid));

        $userdegree = $DB->get_record_sql("SELECT DISTINCT data  FROM {user_info_data} as fdata

        left join {user_info_field} as fieldss on fdata.fieldid = fieldss.id where fieldss.shortname =

        'degree' and fdata.userid = '$user->id'");

        $userdegreename = $userdegree->data;

        $userdepartment = $user->department;

        $userinstitution = $user->institution;

        $ssopluginconfig = get_config('auth_leeloolxp_tracking_sso');

        $studentnumcombinationsval = $ssopluginconfig->student_num_combination;

        $studentdbsetarr = array();

        for ($si = 1; $studentnumcombinationsval >= $si; $si++) {
            $studentpositionmoodle = 'student_position_moodle_' . $si;

            $mstudentrole = $ssopluginconfig->$studentpositionmoodle;

            $studentinstitution = 'student_institution_' . $si;

            $mstudentinstitution = $ssopluginconfig->$studentinstitution;

            $studentdepartment = 'student_department_' . $si;

            $mstudentdepartment = $ssopluginconfig->$studentdepartment;

            $studentdegree = 'student_degree_' . $si;

            $mstudentdegree = $ssopluginconfig->$studentdegree;

            $studentdbsetarr[$si] = $mstudentrole . "_" . $mstudentinstitution . "_" .

                $mstudentdepartment . "_" . $mstudentdegree;
        }

        $userstudentinfo = $roleid . "_" . $userinstitution . "_" . $userdepartment . "_" .

            $userdegreename;

        $matchedvalue = array_search($userstudentinfo, $studentdbsetarr);

        if ($matchedvalue) {
            $tcolnamestudent = 'student_position_t_' . $matchedvalue;
            $teamniostudentrole = $ssopluginconfig->$tcolnamestudent;
            if (!empty($teamniostudentrole)) {
                $teamniorole = $teamniostudentrole;
            }
            $usertype = 'student';
        } else {

            $teachernumcombinationsval = $ssopluginconfig->teachernumcombination;

            $teacherdbsetarr = array();

            for ($si = 1; $teachernumcombinationsval >= $si; $si++) {
                $teacherpositionmoodle = 'teacher_position_moodle_' . $si;

                $mteacherrole = $ssopluginconfig->$teacherpositionmoodle;

                $teacherinstitution = 'teacher_institution_' . $si;

                $mteacherinstitution = $ssopluginconfig->$teacherinstitution;

                $teacherdepartment = 'teacher_department_' . $si;

                $mteacherdepartment = $ssopluginconfig->$teacherdepartment;

                $teacherdegree = 'teacher_degree_' . $si;

                $mteacherdegree = $ssopluginconfig->$teacherdegree;

                $teacherdbsetarr[$si] = $mteacherrole . "_" . $mteacherinstitution . "_" .

                    $mteacherdepartment . "_" . $mteacherdegree;
            }

            $userteacherinfo = $roleid . "_" . $userinstitution . "_" . $userdepartment . "_" .

                $userdegreename;

            $matchedvalueteacher = array_search($userteacherinfo, $teacherdbsetarr);

            if ($matchedvalueteacher) {
                $tcolnameteacher = 'teacher_position_t_' . $matchedvalueteacher;

                $teamnioteacherrole = $ssopluginconfig->$tcolnameteacher;

                if (!empty($teamnioteacherrole)) {
                    $teamniorole = $teamnioteacherrole;
                }

                $usertype = 'teacher';
            } else {

                $usertype = 'student';

                $teamniorole = $ssopluginconfig->default_student_position;
            }
        }

        if ($usertype == 'student') {
            $cancreateuser = $ssopluginconfig->web_new_user_student;

            $userapproval = $ssopluginconfig->required_aproval_student;

            $userdesignation = $teamniorole;
        } else {

            if ($usertype == 'teacher') {
                $cancreateuser = $ssopluginconfig->web_new_user_teacher;

                $userdesignation = $teamniorole;

                $userapproval = $ssopluginconfig->required_aproval_teacher;
            }
        }

        $configenroll = get_config('tool_leeloolxp_sync');

        $liacnsekey = $configenroll->leeloolxp_synclicensekey;

        $postdata = array('license_key' => $liacnsekey);

        $url = 'https://leeloolxp.com/api_moodle.php/?action=page_info';

        $curl = new curl;

        $options = array(

            'CURLOPT_RETURNTRANSFER' => true,

            'CURLOPT_HEADER' => false,

            'CURLOPT_POST' => count($postdata),

        );

        if (!$output = $curl->post($url, $postdata, $options)) {
            return true;
        }

        $infoteamnio = json_decode($output);

        if ($infoteamnio->status != 'false') {
            $teamniourl = $infoteamnio->data->install_url;
        } else {

            $teamniourl = '';

            return false;
        }

        $lastlogin = date('Y-m-d h:i:s', $user->lastlogin);

        $fullname = ucfirst($user->firstname) . " " . ucfirst($user->middlename) . " "

        . ucfirst($user->lastname);

        $city = $user->city;

        $country = $user->country;

        $timezone = $user->timezone;

        $skype = $user->skype;

        $idnumber = $user->idnumber;

        $institution = $user->institution;

        $department = $user->department;

        $phone = $user->phone1;

        $moodlephone = $user->phone2;

        $adress = $user->address;

        $firstaccess = $user->firstaccess;

        $lastaccess = $user->lastaccess;

        $lastlogin = $lastlogin;

        $lastip = $user->lastip;

        $description = $user->description;

        $descriptionofpic = $user->imagealt;

        $alternatename = $user->alternatename;

        $webpage = $user->url;

        $moodleurlpic = new moodle_url('/user/pix.php/' . $user->id . '/f1.jpg');

        $moodlepicdata = file_get_contents($moodleurlpic);

        $postdata = array(
            'email' => $user->email,
            'username' => $user->username,
            'fullname' => $fullname,
            'courseid' => $enrolmentdata->courseid,
            'designation' => $userdesignation,
            'user_role' => $teamniorole,
            'user_approval' => $userapproval,
            'can_user_create' => $cancreateuser,
            'user_type' => $usertype,
            'city' => $city,
            'country' => $country,
            'timezone' => $timezone,
            'skype' => $skype,
            'idnumber' => $idnumber,
            'institution' => $institution,
            'department' => $department,
            'phone' => $phone,
            'moodle_phone' => $moodlephone,
            'adress' => $adress,
            'firstaccess' => $firstaccess,
            'lastaccess' => $lastaccess,
            'lastlogin' => $lastlogin,
            'lastip' => $lastip,
            'user_profile_pic' => urlencode($moodlepicdata),
            'user_description' => $description,
            'picture_description' => $descriptionofpic,
            'alternate_name' => $alternatename,
            'web_page' => $webpage,
        );

        $url = $teamniourl . '/admin/sync_moodle_course/enrolment_newuser';

        $curl = new curl;

        $options = array(

            'CURLOPT_RETURNTRANSFER' => true,

            'CURLOPT_HEADER' => false,

            'CURLOPT_POST' => count($postdata),

        );

        if (!$output = $curl->post($url, $postdata, $options)) {
            return true;
        }
    }

    /**
     * Triggered when category delete.
     *
     * @param \core\event\course_category_deleted $events
     */
    public static function course_category_delete(\core\event\course_category_deleted $events) {

        $configenroll = get_config('tool_leeloolxp_sync');
        $liacnsekey = $configenroll->leeloolxp_synclicensekey;
        $postdata = array('license_key' => $liacnsekey);
        $url = 'https://leeloolxp.com/api_moodle.php/?action=page_info';
        $curl = new curl;
        $options = array(
            'CURLOPT_RETURNTRANSFER' => true,
            'CURLOPT_HEADER' => false,
            'CURLOPT_POST' => count($postdata),
        );
        if (!$output = $curl->post($url, $postdata, $options)) {
            return true;
        }
        $infoteamnio = json_decode($output);

        if ($infoteamnio->status != 'false') {
            $teamniourl = $infoteamnio->data->install_url;
            $postdata = array('moodle_cat_id' => $events->get_data()['objectid']);
            $url = $teamniourl . '/admin/sync_moodle_course/delete_category_from_moodle/';
            $curl = new curl;
            $options = array(
                'CURLOPT_RETURNTRANSFER' => true,
                'CURLOPT_HEADER' => false,
                'CURLOPT_POST' => count($postdata),
            );

            if (!$output = $curl->post($url, $postdata, $options)) {
                return true;
            }
            die;
        }
    }
}
